# Tips and Tricks
This file contains tips on working with vim

## Comparing recovered .swp file (recovered) with the current.

+ First open the file then on the prompt select 'r'
+ In the recovered file `:sav! ./.recovered` to save it as '.recovered' in the current directory
+ Exit the file and `kdiff3 original .recovered` to see the difference between the two and solve the conflicts

## File/tab management

To open another file while still in vim do
```
:tabe <filepath>
```
## Find and Replace

You can find and replace text using the `:substitute` (`:s`) command  
Basic form:  
`:[range]s/{pattern}/{string}/[flags] [count]`  

The command searches each line in `[range]` for a `{pattern}`, and replaces it with a `{string}`. `[count]` is a positive integer that multiplies the command.
